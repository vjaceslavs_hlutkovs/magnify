# magnify

> Magnify Slack Bot Configurable Landing Page

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# SASS compilation (assuming you have SASS installed on your system)
sass assets/scss/style.scss assets/style.css
```

Configation of the sections for the "pages" located in: `./app/config.js`
