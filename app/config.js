export default {
    // List of webpage sections
    sections: {

        comics: {
            image: '/assets/images/comics.png'
        },

        // Video Section
        video: {
            // ID of the YouTube video that you want to be displayed
            youtubeID: 'HEXWRTEbj1I'
        },

        // Steps Section (Next to video)
        steps: {

            // List of steps that you want to be displayed
            list: [
                'Write a message, question or poll',
                'Select delivery group (for example, everybody)',
                'Magnify will create personal message one-by-one to everybody',
                'When people reply, you see all active conversations in dashboard. You can reply to them'
            ]
        },

        // Pricing section
        pricing: {

            /*
                List of plans with it's information where
                - title: Name of the plan
                - price: List of the plan prices
                - price.annual: Annual price of the plan
                - price.monthly: Monthly price of the plan
            */
            plans: [
                {
                    title: 'Cloud',
                    price: {
                        annual: 1.49,
                        monthly: 2.50
                    }
                },
                {
                    title: 'Enterprise',
                    price: {
                        annual: 2.98,
                        monthly: 5.00
                    }
                },
                {
                    title: 'Enterprise',
                    price: {
                        annual: 2.98,
                        monthly: 5.00
                    }
                }
            ]
        },

        // Story section
        story: {

            /*
                Content of the story section, basically small article where
                - title: Title of the story (Goes right under the Story)
                - body: Actual story (HTML allowed)
                - author: Information about the author
                - author.name: Name of the author
                - author.image: Photo of the author
            */
            content: {
                title: 'This is a story about how the Magnify was created',
                body: '<p>Magnify idea authour and developer, Alexandrs, joined our company, when we were 10 people and since that time team has grown to 150.</p><p>We have variety of tools to keep large team engaged and in sync - SpeakUp, Slack, Confluence, Facebook group, physical boards and large screens with latest news, but they lacked personalisation.</p>',
                author: {
                    name: 'Aleksandrs',
                    image: 'author.jpg'
                }
            }
        },

        // Statistic section
        stats: {
            /*
                List of items that will be displayed in the statistic section where
                - value: Value that will be written on the "progress" bar
                - percent: Percent of the width for the progress bar
                - title: Title that will be displayed above progress bar
                - icon: Icon of the item
            */
            items: [
                {
                    value: '7%',
                    percent: 8,
                    title: 'One e-mail to <b>@all</b> domain',
                    icon: 'stats-mail.png'
                },
                {
                    value: '12%',
                    percent: 14,
                    title: 'One slack message to <b>#general</b> channel',
                    icon: 'stats-slack.png'
                },
                {
                    value: '81%',
                    percent: 100,
                    title: 'The same text sent via <b>Magnify</b>',
                    icon: 'stats-magnify.png'
                }
            ]
        },

        // Contact section
        contact: {
            // AJAX URL for the form submit action
            submitUrl: 'http://localhost:8080'
        }
    }
}
